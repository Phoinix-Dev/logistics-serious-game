#pragma once

#include "Engine.h"
#include "ShipGameInstance.h"
#include "EFreightType.h"
#include "GameFramework/PlayerController.h"
#include "ShipPlayerController.generated.h"

class AHarbor;
class AShipPawn;

UCLASS()
class SERIOUS_API AShipPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	/**
	 * @brief The camera placed in the level has to have this tag, 
	 * so that it can be detected automatically
	 */
	UPROPERTY(EditDefaultsOnly)
	FName CameraTag = "TopCamera";

	UPROPERTY(BlueprintReadWrite, Replicated)
	FName PlayerName;

	// Delegate to broadcast that win-relevant values have changed
	DECLARE_MULTICAST_DELEGATE(FWinRelevantValuesUpdated);
	FWinRelevantValuesUpdated WinRelevantValuesUpdated;

	// Delegate to broadcast that the view mode has changed
	DECLARE_MULTICAST_DELEGATE_OneParam(FViewModeChanged, bool);
	FViewModeChanged ViewModeChanged;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	TArray<AHarbor *> GetVisitedHarbors();

	/**
	 * @brief Spawn the pawn of the player, depending on the freightmode
	 * This is executed on the server
	 * 
	 * @param FreightType The freight mode of the level
	 */
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void SpawnPawn(EFreightType FreightType);
	void SpawnPawn_Implementation(EFreightType FreightType);

	/**
	 * @brief The Player has visited a harbour
	 * This is executed on the server
	 * 
	 * @param Harbor The harbour that has been visited
	 */
	UFUNCTION(Server, Reliable)
	void AddVisitedHarbor(AHarbor *Harbor);
	void AddVisitedHarbor_Implementation(AHarbor *Harbor);

	/**
	 * @brief Finish the route for this controller
	 * 
	 */
	UFUNCTION(Server, Reliable)
	void Finish();
	void Finish_Implementation();

	/**
	 * @brief Returns whether the harbour has already been visited
	 * 
	 * @param Harbor harbour the check
	 * @return true harbour visited
	 * @return false harbor not visited
	 */
	bool HasVisited(AHarbor *Harbor);

	/**
	 * @brief Return whether the player has finished the route
	 * 
	 * @return true 
	 * @return false 
	 */
	bool HasFinished();

	/**
	 * @brief Shows the Ranking Screen
	 * Only execute on a client
	 */
	UFUNCTION(Reliable, Client)
	void ShowRanking();
	void ShowRanking_Implementation();

	/**
 	* @brief Set the Playername for this controller
 	* 
	 */
	UFUNCTION(Server, Reliable)
	void SetPlayerName(FName LocalPlayerName);
	void SetPlayerName_Implementation(FName LocalPlayerName);

	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<AHarbor *> VisitedHarbors;

protected:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

private:
	ACameraActor *TopCamera = nullptr;

	class AShipPawn *ShipPlayerPawn;

	class UShipGameInstance *GameInstance;

	class AShipGameMode *GameMode;

	bool Finished = false;

	/**
	 * @brief Switch to the other viewmode
	 * 
	 */
	void SwitchCamera();
};
