#include "MenuLevelScript.h"

void AMenuLevelScript::BeginPlay()
{
    Super::BeginPlay();
    GameInstance = Cast<UShipGameInstance>(GetGameInstance());
    check(GameInstance);
    // Sets the Main Menu
    GameInstance->ChangeMenuWidget(MainMenu);
}