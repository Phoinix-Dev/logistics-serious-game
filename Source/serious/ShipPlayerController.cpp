#include "ShipPlayerController.h"
#include "ShipGameState.h"
#include "Harbor.h"
#include "ShipPawn.h"
#include "ShipGameMode.h"

void AShipPlayerController::BeginPlay()
{
    GameInstance = Cast<UShipGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

    // Get the tagged camera for the topview mode
    TArray<AActor *> CameraActors;
    UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), ACameraActor::StaticClass(), CameraTag, CameraActors);

    // It is required to have one
    if (ensure(CameraActors.Num() == 1))
    {
        TopCamera = Cast<ACameraActor>(CameraActors[0]);
    }

    if (HasAuthority())
    {
        GameMode = Cast<AShipGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
        WinRelevantValuesUpdated.AddUObject(GameMode, &AShipGameMode::CheckWinConditions);
    }

    // We set the name of the local player by the name in the game instance
    if (IsLocalController())
    {
        SetPlayerName(GameInstance->PlayerName);
    }
}

void AShipPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AShipPlayerController::SwitchCamera);
}

void AShipPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    DOREPLIFETIME(AShipPlayerController, PlayerName);
    DOREPLIFETIME(AShipPlayerController, VisitedHarbors);
}

void AShipPlayerController::SpawnPawn_Implementation(EFreightType FreightType)
{
    AShipGameMode *Gm = Cast<AShipGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
    if (HasAuthority())
    {
        Gm->SpawnPawn(this, FreightType);
    }
    ShipPlayerPawn = Cast<AShipPawn>(GetPawnOrSpectator());
    // After spawning the pawn, we have the pawn point of view, no mouse is not required anymore
    bShowMouseCursor = 0;
}

void AShipPlayerController::SwitchCamera()
{
    if (ShipPlayerPawn == nullptr)
    {
        ShipPlayerPawn = Cast<AShipPawn>(GetPawnOrSpectator());
    }
    check(ShipPlayerPawn);
    /**
     * Change to the other point of view with a blend and broadcast 
     * that the point of view has changed
     */
    if (GetViewTarget() == ShipPlayerPawn)
    {
        Super::SetViewTargetWithBlend(TopCamera, 1.f, EViewTargetBlendFunction::VTBlend_Linear, 0, false);
        ViewModeChanged.Broadcast(true);
    }
    else
    {
        Super::SetViewTargetWithBlend(ShipPlayerPawn, 1.f, EViewTargetBlendFunction::VTBlend_Linear, 0, false);
        ViewModeChanged.Broadcast(false);
    }
}

void AShipPlayerController::AddVisitedHarbor_Implementation(AHarbor *Harbor)
{
    if (!VisitedHarbors.Contains(Harbor))
    {
        VisitedHarbors.Add(Harbor);
    }
    if (GetVisitedHarbors().Num() == GameMode->AllHarbors.Num())
    {
        // Check if the current Player has Visited all Harbors yet, if not break
        for (AHarbor *H : GameMode->AllHarbors)
        {
            if (!GetVisitedHarbors().Contains(H))
            {
                break;
            }
        }
        // Player has visited all Harbors
        if (!HasFinished())
        {
            // Finish the route for the Player
            Finish();
        }
    }
    WinRelevantValuesUpdated.Broadcast();
}

void AShipPlayerController::Finish_Implementation()
{
    AShipGameState *GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));
    GameState->FinishRoute(this);
}

TArray<AHarbor *> AShipPlayerController::GetVisitedHarbors()
{
    return VisitedHarbors;
}

bool AShipPlayerController::HasVisited(AHarbor *Harbor)
{
    return VisitedHarbors.Contains(Harbor);
}

bool AShipPlayerController::HasFinished()
{
    AShipGameState *GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));
    return GameState->FinishedPlayers.Contains(this);
}

void AShipPlayerController::ShowRanking_Implementation()
{
    /**
     * Show the rankling screen and deactivate the input and audio
     */
    ShipPlayerPawn = Cast<AShipPawn>(GetPawnOrSpectator());
    GameInstance->ShowRanking();
    ShipPlayerPawn->DisableInput(this);
    ShipPlayerPawn->AudioComponent->Deactivate();

    // Again in a menu, the cursor is required
    bShowMouseCursor = 1;
}

void AShipPlayerController::SetPlayerName_Implementation(FName LocalPlayerName)
{
    this->PlayerName = LocalPlayerName;
}
