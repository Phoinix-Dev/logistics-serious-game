#include "ShipPawn.h"
#include "ShipGameState.h"
#include "EFreightType.h"

AShipPawn::AShipPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	ShipStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipStaticMesh"));
	ShipSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("ShipSpringArm"));
	ShipCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ShipCamera"));
	VisualsPlane = CreateDefaultSubobject<UShipPawnVisualsPlane>(TEXT("VisualsPlane"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));

	this->SetRootComponent(ShipStaticMesh);
	ShipSpringArm->SetupAttachment(ShipStaticMesh);
	ShipCamera->SetupAttachment(ShipSpringArm);
	VisualsPlane->SetupAttachment(ShipStaticMesh);
	AudioComponent->SetupAttachment(ShipStaticMesh);
}

void AShipPawn::BeginPlay()
{
	Super::BeginPlay();
	ensure(ContainerVessel != nullptr);
	ensure(Tanker != nullptr);
	ensure(BulkCarrier != nullptr);
	ensure(RoRoShip != nullptr);

	AShipGameState* GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));

	// Sets the static mesh based on the level freight mode
	switch (GameState->FreightMode)
	{
	case EFreightType::Container:
		ShipStaticMesh->SetStaticMesh(ContainerVessel);
		break;
	case EFreightType::Oil:
		ShipStaticMesh->SetStaticMesh(Tanker);
		break;
	case EFreightType::Coal:
		ShipStaticMesh->SetStaticMesh(BulkCarrier);
		break;
	case EFreightType::Vehicle:
		ShipStaticMesh->SetStaticMesh(RoRoShip);
		break;
	default:
		UE_LOG(LogTemp, Error, TEXT("ShipPawn: Schiffstyp nicht definiert"));
	}

	FTimerHandle Handler;
	GetWorldTimerManager().SetTimer(Handler, this, &AShipPawn::SetInitialized, 2, false);
}

void AShipPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float ForwardInput = GetInputAxisValue(FName("MoveForward"));
	if (Initialized)
	{
		// Set the pitch multiplier on the the current input
		AudioComponent->SetPitchMultiplier(FMath::Clamp<float>((ForwardInput * 2), 1.f, 4.f));


		// If we are not locally controlled, we set the location top the target location
		// The Z Axis is ignored
		if (!IsLocallyControlled())
		{
			// ! Simulate Physics Problem, deactivated, therefore activate it again
			ShipStaticMesh->SetSimulatePhysics(true);
			SetActorLocation(FVector(TargetLocation.X, TargetLocation.Y, GetActorLocation().Z));
			SetActorRotation(TargetRotation, ETeleportType::None);
		}
	}
}

void AShipPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(AShipPawn, TargetRotation);
	DOREPLIFETIME(AShipPawn, TargetLocation);
}

void AShipPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AShipPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShipPawn::MoveRight);
}

void AShipPawn::MoveForward(float Input)
{
	// ! Simulate Physics Problem, deactivated, therefore activate it again
	ShipStaticMesh->SetSimulatePhysics(true);
	// Move Forward
	ShipStaticMesh->AddForceAtLocationLocal(FVector((ForwardMultiplier * Input), 0, 0),
	                                        FVector(0, 0, 0));
	/**
	 * To replicate the movement to the server
	 * To avoid having complicated physics replication we simply give the location by argument to the server
	 * ! If cheat safety is important, this can not be done like this
	 */
	ServerMovedForward(GetActorLocation());
}

void AShipPawn::ServerMovedForward_Implementation(FVector NewTargetLocation)
{
	// We set target location, will be set every tick on all the other players in the session
	TargetLocation = NewTargetLocation;
}

void AShipPawn::MoveRight(float Input)
{
	FRotator NewTargetRotation = GetActorRotation();
	NewTargetRotation.Yaw = NewTargetRotation.Yaw + Input * RotationMultiplier * GetWorld()->GetDeltaSeconds();
	ServerMoveRight(NewTargetRotation);
	SetActorRotation(NewTargetRotation);
}

void AShipPawn::ServerMoveRight_Implementation(FRotator NewTargetRotation)
{
	TargetRotation = NewTargetRotation;
}

void AShipPawn::SetInitialized()
{
	Initialized = true;
}
