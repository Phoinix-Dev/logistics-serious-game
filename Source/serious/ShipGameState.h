#pragma once

#include "Engine.h"
#include "Engine/Public/Net/UnrealNetwork.h"
#include "GameFramework/GameStateBase.h"
#include "ShipPlayerController.h"
#include "EFreightType.h"
#include "FRankListEntry.h"
#include "ShipGameState.generated.h"

UCLASS()
class SERIOUS_API AShipGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	AShipGameState();

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<EFreightType> FreightMode;


	/**
	 * @brief Returns the Players who finished their Route - SERVER only
	 * 
	 * @return PlayerController of the finished players
	 */
	UPROPERTY(BlueprintReadWrite)
	TArray<AShipPlayerController*> FinishedPlayers;

	/**
	 * @brief Can be Used for UI on Client
	 * 
	 * @return The finished Players in Struct form
	 */
	UPROPERTY(BlueprintReadWrite, Replicated)
	TArray<FRankListEntry> Ranking;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

	/**
	 * @brief Finished the route for the given Player
	 * 
	 * @param PlayerController The Player that finished the Route
	 */
	UFUNCTION(Server, Reliable)
	void FinishRoute(AShipPlayerController *PlayerController);
	void FinishRoute_Implementation(AShipPlayerController *PlayerController);

};
