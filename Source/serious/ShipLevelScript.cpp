#include "ShipLevelScript.h"
#include "ShipGameState.h"

void AShipLevelScript::BeginPlay()
{
    Super::BeginPlay();
    GameInstance = Cast<UShipGameInstance>(GetGameInstance());
    check(GameInstance);

    AShipGameState *GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));

    // Show the correct PawnSelector Widget based on the level mode
    switch (GameState->FreightMode)
    {
    case EFreightType::Container:
        GameInstance->ChangeMenuWidget(GameInstance->Container);
        break;
    case EFreightType::Oil:
        GameInstance->ChangeMenuWidget(GameInstance->Oil);
        break;
    case EFreightType::Coal:
        GameInstance->ChangeMenuWidget(GameInstance->Coal);
        break;
    case EFreightType::Vehicle:
        GameInstance->ChangeMenuWidget(GameInstance->Vehicles);
        break;
    default:
        UE_LOG(LogTemp, Error, TEXT("ShipPawn: Schiffstyp nicht definiert"));
    }
}

void AShipLevelScript::SetFreightMode(EFreightType FreightType)
{
    AShipGameState *GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));
    GameState->FreightMode = FreightType;
}
