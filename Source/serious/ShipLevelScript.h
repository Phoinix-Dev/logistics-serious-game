#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "ShipGameInstance.h"
#include "EFreightType.h"
#include "ShipLevelScript.generated.h"

/**
 * @brief The Level Script for the Gameplay Maps
 * 
 */
UCLASS()
class SERIOUS_API AShipLevelScript : public ALevelScriptActor
{
	GENERATED_BODY()

public:
	virtual void BeginPlay();

	UFUNCTION(BlueprintCallable)
	void SetFreightMode(EFreightType FreightType);

private:
	UShipGameInstance *GameInstance;
};
