#pragma once

#include "CoreMinimal.h"
#include "EFreightType.generated.h"

UENUM(BlueprintType)
enum EFreightType
{
	Container,
	Coal,
	Vehicle,
	Oil

};
