#include "Rank.h"
#include "FRankListEntry.h"
#include "ShipGameState.h"

void URank::NativeConstruct()
{
    Super::NativeConstruct();
    GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));
    ensure(GameState != nullptr);
}

void URank::NativeTick(const FGeometry &MyGeometry, float DeltaTime)
{
    Super::NativeTick(MyGeometry, DeltaTime);
    /**
     * Refreshing the ranking every tick
     */
    GenerateRanking();
}

void URank::GenerateRanking()
{
    // Remove all entries to repopulate
    ClearList();
    if (GameState != nullptr)
    {
        /**
         * Wrap each Ranking elements and add it to the list
         * Wrapping it is required as the Rank widget can only deal
         * with @FRankedListEntryWrapper
         */
        for (FRankListEntry Entry : GameState->Ranking)
        {
            UFRankedListEntryWrapper *Wrapper = NewObject<UFRankedListEntryWrapper>();
            Wrapper->Entry = Entry;
            AddItemToList(Wrapper);
        }
    }
}

void URank::BackToMenu()
{
    UGameplayStatics::OpenLevel(GetWorld(), FName("Menu"), true);
}