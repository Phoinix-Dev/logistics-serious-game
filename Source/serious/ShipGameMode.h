#pragma once

#include "CoreMinimal.h"
#include "Harbor.h"
#include "Blueprint/UserWidget.h"
#include "EFreightType.h"
#include "ShipPlayerController.h"
#include "ShipGameState.h"
#include "GameFramework/GameModeBase.h"
#include "ShipGameMode.generated.h"

UCLASS()
class SERIOUS_API AShipGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	TArray<AHarbor *> AllHarbors;

	virtual void BeginPlay();

	/**
	 * @brief Checks whether a player has finished the route
	 */
	UFUNCTION(Server, Reliable)
	void CheckWinConditions();
	void CheckWinConditions_Implementation();

	void SpawnPawn(AShipPlayerController *Pc, EFreightType FreightType);

private:
	AShipGameState *GameState;

	TArray<APlayerStart *> PlayerStarts;
};
