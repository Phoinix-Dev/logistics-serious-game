#include "ShipPawnVisualsPlane.h"

void UShipPawnVisualsPlane::BeginPlay()
{
    Super::BeginPlay();
    PlayerController = Cast<AShipPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
    PlayerController->ViewModeChanged.AddUObject(this, &UShipPawnVisualsPlane::TopView);

    // Save the initial scale to reset after changed
    InitScale = GetRelativeScale3D();
    // Normally not visible
    SetHiddenInGame(true);
}

void UShipPawnVisualsPlane::TopView(bool TopView)
{
    if (!Initialized)
    {
        Initialize();
    }

    if (TopView)
    {
        SetHiddenInGame(false);
    }
    else
    {
        SetHiddenInGame(true);
    }
}

void UShipPawnVisualsPlane::Initialize()
{
    if (!Initialized)
    {
        // Dynamic Material coloring based on the player
        APawn *Pawn = Cast<APawn>(GetOwner());
        if (ensure(SelfMaterial != nullptr) && Pawn->GetController() == PlayerController)
        {
            UMaterialInstanceDynamic *DynamicMaterial = UMaterialInstanceDynamic::Create(SelfMaterial, this);
            SetMaterial(0, DynamicMaterial);
            DynamicMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Black);
        }
        else if (ensure(OtherPlayerMaterial != nullptr))
        {
            UMaterialInstanceDynamic *DynamicMaterial = UMaterialInstanceDynamic::Create(OtherPlayerMaterial, this);
            SetMaterial(0, DynamicMaterial);
            DynamicMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Red);
        }
        Initialized = true;
    }
}