#include "ShipGameMode.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "Harbor.h"

void AShipGameMode::BeginPlay()
{
    Super::BeginPlay();

    GameState = GetGameState<AShipGameState>();
    // Get all Harbors in the Map
    TArray<AActor *> Harbors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHarbor::StaticClass(), Harbors);
    for (AActor *Actor : Harbors)
    {
        AHarbor *Harbor = Cast<AHarbor>(Actor);
        AllHarbors.Add(Harbor);
    }

    TArray<AActor *> FoundPlayerStarts;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), FoundPlayerStarts);
    for (AActor *Actor : FoundPlayerStarts)
    {
        APlayerStart *Start = Cast<APlayerStart>(Actor);
        this->PlayerStarts.Add(Start);
    }
}

void AShipGameMode::CheckWinConditions_Implementation()
{
    for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; Iterator++)
    {
        AShipPlayerController *PlayerController = Cast<AShipPlayerController>(Iterator->Get());
        check(PlayerController != nullptr);
        if (PlayerController->GetVisitedHarbors().Num() == AllHarbors.Num())
        {
            // Check if the current Player has Visited all Harbors yet, if not break
            for (AHarbor *Harbor : AllHarbors)
            {
                if (!PlayerController->GetVisitedHarbors().Contains(Harbor))
                {
                    break;
                }
            }
            // Player has visited all Harbors
            if (!PlayerController->HasFinished())
            {
                // Finish the route for the Player
                GameState->FinishRoute(PlayerController);
            }
        }
    }
}

void AShipGameMode::SpawnPawn(AShipPlayerController *Pc, EFreightType FreightType)
{
    if (GameState->FreightMode == FreightType && ensure(PlayerStarts.Num() > 0))
    {
        RestartPlayerAtPlayerStart(Pc, PlayerStarts[0]);
        PlayerStarts.RemoveAt(0);
    }
}