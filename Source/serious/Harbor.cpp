#include "Harbor.h"
#include "ShipPawn.h"
#include "ShipPlayerController.h"

AHarbor::AHarbor()
{
	PrimaryActorTick.bCanEverTick = true;

	HarborStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HarborStaticMesh"));
	this->SetRootComponent(HarborStaticMesh);

	AcceptanceBox = CreateDefaultSubobject<UBoxComponent>(TEXT("AcceptanceBox"));
	AcceptanceBox->SetupAttachment(RootComponent);

	AcceptanceBox->OnComponentBeginOverlap.AddDynamic(this, &AHarbor::OnBoxBeginOverlap);

	VisualsPlane = CreateDefaultSubobject<UVisualsPlaneComponent>(TEXT("VisualsPlane"));
	VisualsPlane->SetupAttachment(AcceptanceBox);
}

void AHarbor::BeginPlay()
{
	Super::BeginPlay();
}

void AHarbor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHarbor::SetHarborVisited(AShipPawn *Pawn)
{
	AShipPlayerController *PlayerController = Cast<AShipPlayerController>(Pawn->GetController());
	if (PlayerController != nullptr)
	{
		PlayerController->AddVisitedHarbor(this);
		VisualsPlane->Visited();
	}
}

void AHarbor::OnBoxBeginOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	AShipPawn *ShipPawn = Cast<AShipPawn>(OtherActor);
	if (ShipPawn != nullptr)
	{
		SetHarborVisited(ShipPawn);
	}
}