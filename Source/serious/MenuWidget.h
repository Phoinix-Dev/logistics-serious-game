#pragma once

#include "CoreMinimal.h"
#include "Engine/Level.h"
#include "Blueprint/UserWidget.h"
#include "ShipGameInstance.h"

#include "MenuWidget.generated.h"

/**
 * @brief The main menu of the game
 * 
 */
UCLASS()
class SERIOUS_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct();

	/**
	 * Widget for joining a multiplayer session
	 */
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> JoinMenu;

	/**
	 * Widget to host a multiplayer session
	 */
	UFUNCTION(BlueprintCallable)
	void Host(FString Adress);

	UFUNCTION(BlueprintCallable)
	void ShowJoinMenu();

	UFUNCTION(BlueprintCallable)
	void ShowMenu(TSubclassOf<UUserWidget> Menu);


	/**
	 * @brief Join any adress
	 * 
	 * @param Adress IP Adress
	 * @param PlayerName Playername
	 */
	UFUNCTION(BlueprintCallable)
	void Join(FString Adress, FName PlayerName);

private:
	UShipGameInstance *GameInstance;
};
