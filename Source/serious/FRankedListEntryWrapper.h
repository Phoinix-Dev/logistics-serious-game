#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FRankListEntry.h"
#include "FRankedListEntryWrapper.generated.h"


UCLASS(BlueprintType)
class SERIOUS_API UFRankedListEntryWrapper : public UObject
{
	GENERATED_BODY()

	public:
	UPROPERTY(BlueprintReadWrite)
	FRankListEntry Entry;
	
};
