#pragma once

#include "Engine.h"
#include "ShipPawnVisualsPlane.h"
#include "Engine/Classes/Components/AudioComponent.h"
#include "GameFramework/Pawn.h"
#include "ShipPawn.generated.h"

UCLASS()
class SERIOUS_API AShipPawn : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent *ShipStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USpringArmComponent *ShipSpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent *ShipCamera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UShipPawnVisualsPlane *VisualsPlane;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAudioComponent *AudioComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh *ContainerVessel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh *BulkCarrier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh *Tanker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh *RoRoShip;

	// The Forward Input is Multiplied by this Value
	UPROPERTY(EditAnywhere)
	float ForwardMultiplier = 99999;

	// The Rotation Input is Multiplied by this Value
	UPROPERTY(EditAnywhere)
	float RotationMultiplier = 0.5;

	// Target location for the movement replication
	UPROPERTY(BlueprintReadWrite, Replicated)
	FVector TargetLocation = FVector(0, 0, 0);

	// Target Rotation for the movement replication
	UPROPERTY(BlueprintReadWrite, Replicated)
	FRotator TargetRotation = FRotator(0, 0, 0);

	AShipPawn();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;

	/**
 	* @brief Executes the physic based forward movement locally and replicates it to the server
 	* 
 	* @param Input Current input
 	*/
	void MoveForward(float Input);

	/**
	 * @brief Sets the target location for the movement replication
	 * @param NewTargetLocation New Location 
	 */
	UFUNCTION(Server, Reliable)
	void ServerMovedForward(FVector NewTargetLocation);
	void ServerMovedForward_Implementation(FVector NewTargetLocation);

	/**
 	* @brief Rotates the pawn locally and replicates it to the server
 	* 
	* @param Input 
	*/
	void MoveRight(float Input);

	UFUNCTION(Server, Reliable)
	void ServerMoveRight(FRotator Rotation);
	void ServerMoveRight_Implementation(FRotator Rotation);

protected:
	virtual void BeginPlay() override;

private:
	bool Initialized = false;

	void SetInitialized();
};
