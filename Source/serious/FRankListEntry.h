#pragma once

#include "Engine.h"
#include "FRankListEntry.generated.h"

USTRUCT(BlueprintType)
struct FRankListEntry
{
    GENERATED_USTRUCT_BODY();

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString PlayerName;
};
