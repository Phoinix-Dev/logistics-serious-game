#pragma once

#include "VisualsPlaneComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Harbor.generated.h"

class AShipPawn;

UCLASS()
class SERIOUS_API AHarbor : public AActor
{
	GENERATED_BODY()

public:
	AHarbor();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* HarborStaticMesh;

	UPROPERTY(EditAnywhere)
	class UBoxComponent* AcceptanceBox;

	UPROPERTY(EditAnywhere)
	class UVisualsPlaneComponent* VisualsPlane;

	virtual void Tick(float DeltaTime) override;

	// Adds this Harbor to the Visited ones for the Overlapping Players
	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                       int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;

private:
	void SetHarborVisited(class AShipPawn* Pawn);
};
