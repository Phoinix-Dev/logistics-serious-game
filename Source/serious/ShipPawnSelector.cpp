#include "ShipPawnSelector.h"
#include "ShipPlayerController.h"
#include "ShipGameInstance.h"

void UShipPawnSelector::NativeConstruct()
{
    Super::NativeConstruct();
    GameInstance = Cast<UShipGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
    GameState = Cast<AShipGameState>(UGameplayStatics::GetGameState(GetWorld()));
    ensure(GameInstance);
    ensure(GameState);
}

void UShipPawnSelector::PawnSelection(EFreightType FreightType)
{
    AShipPlayerController *Pc = Cast<AShipPlayerController>(this->GetOwningPlayer());

    check(Pc);
    // If correct Ship (defined in GameState) was selected on the Client, the game starts
    // Otherwise, timepenalty
    if (FreightType != GameState->FreightMode)
    {
        Enabled = false;
        GameState->GetWorldTimerManager().SetTimer(TimeHandler, this, &UShipPawnSelector::Enable, 10.0f, false);
    }
    else
    {
        Pc->SpawnPawn(FreightType);
        GameInstance->ChangeMenuWidget(nullptr);
    }
}

void UShipPawnSelector::Enable()
{
    Enabled = true;
}

bool UShipPawnSelector::IsEnabled()
{
    return Enabled;
}
