#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MenuGameMode.generated.h"

/**
 * @brief The GameMode used in the Menus
 * Currently only settings the playercontroller
 */
UCLASS()
class SERIOUS_API AMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMenuGameMode();
	
};
