#pragma once

#include "CoreMinimal.h"
#include "Rank.h"
#include "Engine/GameInstance.h"
#include "Blueprint/UserWidget.h"
#include "ShipGameInstance.generated.h"

UCLASS()
class SERIOUS_API UShipGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	UUserWidget *CurrentWidget;

	/**
	 * Widgets for the Pawn selector
 	*/
	UPROPERTY(EditAnywhere, Category = "Serious UI")
	TSubclassOf<UUserWidget> Container;

	UPROPERTY(EditAnywhere, Category = "Serious UI")
	TSubclassOf<UUserWidget> Coal;

	UPROPERTY(EditAnywhere, Category = "Serious UI")
	TSubclassOf<UUserWidget> Oil;

	UPROPERTY(EditAnywhere, Category = "Serious UI")
	TSubclassOf<UUserWidget> Vehicles;

	UPROPERTY(EditAnywhere, Category = "Serious UI")
	TSubclassOf<URank> RankingWidget;

	/**
     * TODO The player name should be definable in the Host Menu, not required for the play test
	 * 
	 */
	UPROPERTY(BlueprintReadWrite, Category = "Serious UI")
	FName PlayerName = "GESCHAFFT";

	/**
	 * @brief Changes the Current Screen Widget
	 * 
	 * @param NewWidgetClass The new Widget to Display
	 */
	UFUNCTION(BlueprintCallable, Category = "Serious UI")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	/**
 	* @brief Adds the Ranking UI to the Viewport
 	*/
	void ShowRanking();
};
