#include "ShipGameInstance.h"

void UShipGameInstance::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
    // Remove the current Widget and add the new one if it exists
    if (CurrentWidget != nullptr)
    {
        CurrentWidget->RemoveFromViewport();
        CurrentWidget = nullptr;
    }
    if (NewWidgetClass != nullptr)
    {
        CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
        if (CurrentWidget != nullptr)
        {
            CurrentWidget->AddToViewport();
        }
    }
}

void UShipGameInstance::ShowRanking()
{
    if (ensure(RankingWidget))
    {
        ChangeMenuWidget(RankingWidget);
    }
}