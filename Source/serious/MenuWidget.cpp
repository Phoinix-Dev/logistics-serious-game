#include "MenuWidget.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"

void UMenuWidget::NativeConstruct()
{
    Super::NativeConstruct();
    GameInstance = Cast<UShipGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
    ensure(GameInstance != nullptr);
}

void UMenuWidget::Host(FString Adress)
{
    UGameplayStatics::OpenLevel(GetWorld(), FName(*Adress), true, FString("?listen"));
}

void UMenuWidget::ShowJoinMenu()
{
    if (ensure(JoinMenu != nullptr))
    {
        GameInstance->ChangeMenuWidget(JoinMenu);
    }
}

void UMenuWidget::ShowMenu(TSubclassOf<UUserWidget> Menu)
{
    if (ensure(Menu != nullptr))
    {
        GameInstance->ChangeMenuWidget(Menu);
    }
}

void UMenuWidget::Join(FString Adress, FName PlayerName)
{
    UGameplayStatics::OpenLevel(GetWorld(), FName(*Adress), true);
    GameInstance->PlayerName = PlayerName;
}