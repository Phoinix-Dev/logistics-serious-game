#pragma once

#include "Engine.h"
#include "FRankListEntry.h"
#include "FRankedListEntryWrapper.h"
#include "Blueprint/UserWidget.h"
#include "Rank.generated.h"

UCLASS()
class SERIOUS_API URank : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct();

	virtual void NativeTick(const FGeometry &MyGeometry, float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> ItemWidget;

	UFUNCTION(BlueprintCallable, Category = "Serious UI")
	void GenerateRanking();

	UFUNCTION(BlueprintCallable, Category = "Serious UI")
	void BackToMenu();

	UFUNCTION(BlueprintImplementableEvent, Category = "Serious UI")
	void AddItemToList(UFRankedListEntryWrapper *Entry);

	/**
	 * @brief Removes all entries from the Ranking
	 * 
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "Serious UI")
	void ClearList();

private:
	class AShipGameState *GameState;
};
