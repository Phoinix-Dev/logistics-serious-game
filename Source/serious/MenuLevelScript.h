#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Blueprint/UserWidget.h"
#include "ShipGameInstance.h"
#include "MenuLevelScript.generated.h"

UCLASS()
class SERIOUS_API AMenuLevelScript : public ALevelScriptActor
{
	GENERATED_BODY()
	
private: 
	UShipGameInstance* GameInstance;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Serious UI")
	TSubclassOf<UUserWidget> MainMenu;

	virtual void BeginPlay();

};
