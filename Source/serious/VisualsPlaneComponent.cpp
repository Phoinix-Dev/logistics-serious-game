#include "VisualsPlaneComponent.h"
#include "Harbor.h"
#include "ShipPlayerController.h"

void UVisualsPlaneComponent::BeginPlay()
{
    Super::BeginPlay();
    PlayerController = Cast<AShipPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

    // React to changed view mode by refreshing the material
    PlayerController->ViewModeChanged.AddUObject(this, &UVisualsPlaneComponent::RefreshMaterial);

    ensure(VisitedMaterial != nullptr);
    ensure(UnvisitedMaterial != nullptr);
    ensure(VisitedTopViewMaterial != nullptr);
    ensure(UnvisitedTopViewMaterial != nullptr);

    // The init scale is saved to go back to after changes
    InitScale = GetRelativeScale3D();
}

void UVisualsPlaneComponent::Visited()
{
    SetMaterial(0, VisitedMaterial);
}

void UVisualsPlaneComponent::RefreshMaterial(bool TopView)
{
    if (TopView)
    {
        if (PlayerController->HasVisited(Cast<AHarbor>(GetOwner())))
        {
            SetMaterial(0, VisitedTopViewMaterial);
        }
        else
        {
            SetMaterial(0, UnvisitedTopViewMaterial);
        }

        // Scale up that it is visible
        SetRelativeScale3D(TopScale);
    }
    else
    {
        if (PlayerController->HasVisited(Cast<AHarbor>(GetOwner())))
        {
            SetMaterial(0, VisitedMaterial);
        }
        else
        {
            SetMaterial(0, UnvisitedMaterial);
        }
        // Scale down to the standard again
        SetRelativeScale3D(InitScale);
    }
}
