#pragma once

#include "Engine.h"
#include "ShipPlayerController.h"
#include "Components/StaticMeshComponent.h"
#include "ShipPawnVisualsPlane.generated.h"

UCLASS()
class SERIOUS_API UShipPawnVisualsPlane : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	void Initialize();

	// Materials for different states
	UPROPERTY(EditAnywhere)
	UMaterialInterface *SelfMaterial;
	UPROPERTY(EditAnywhere)
	UMaterialInterface *OtherPlayerMaterial;

private:
	class AShipPlayerController *PlayerController;

	// Init Scale used to reset after changes
	FVector InitScale;

	bool Initialized;

	void TopView(bool TopView);
};
