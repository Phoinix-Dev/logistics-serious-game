#include "ShipGameState.h"

AShipGameState::AShipGameState()
{
    bReplicates = true;
}

void AShipGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(AShipGameState, Ranking);
}

void AShipGameState::FinishRoute_Implementation(AShipPlayerController *PlayerController)
{
    FRankListEntry Entry;
    Entry.PlayerName = PlayerController->PlayerName.ToString();
    Ranking.Add(Entry);
    FinishedPlayers.Add(PlayerController);
    PlayerController->ShowRanking();
    UE_LOG(LogTemp, Error, TEXT("Finished Route"));
}
