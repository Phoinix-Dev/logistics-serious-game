#pragma once

#include "Engine.h"
#include "Components/StaticMeshComponent.h"
#include "VisualsPlaneComponent.generated.h"

UCLASS()
class SERIOUS_API UVisualsPlaneComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	// Static Materials for different states
	UPROPERTY(EditAnywhere)
	UMaterialInterface *VisitedMaterial;
	UPROPERTY(EditAnywhere)
	UMaterialInterface *UnvisitedMaterial;
	UPROPERTY(EditAnywhere)
	UMaterialInterface *VisitedTopViewMaterial;
	UPROPERTY(EditAnywhere)
	UMaterialInterface *UnvisitedTopViewMaterial;

	// Scaling in Top Mode
	UPROPERTY(EditAnywhere)
	FVector TopScale = FVector(3.f, 3.f, 3.f);

	virtual void BeginPlay() override;

	void Visited();

private:
	class AShipPlayerController *PlayerController;

	// Init Scale used to reset after changes
	FVector InitScale;

	/**
	 * @brief Sets the Material for the given mode 
	 * 
	 * @param TopView Mode to set the materials for
	 */
	void RefreshMaterial(bool TopView);
};
