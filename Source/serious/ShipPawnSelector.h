#pragma once

#include "Engine.h"
#include "ShipGameMode.h"
#include "EFreightType.h"
#include "Blueprint/UserWidget.h"
#include "ShipPawnSelector.generated.h"

UCLASS()
class SERIOUS_API UShipPawnSelector : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;


	UFUNCTION(BlueprintCallable)
	/**
	 * @brief Checks whether the selected Freighttype is correct
	 * If it is correct, the Ship will be spawned, otherwise
	 * time penalty
	 * 
	 * @param FreightType Freight of the selected ship
	 */
	void PawnSelection(EFreightType FreightType);

	UFUNCTION(BlueprintCallable)
	bool IsEnabled();

private:
	FTimerHandle TimeHandler;
	bool Enabled = true;

	class UShipGameInstance *GameInstance;
	class AShipGameState *GameState;

	void Enable();
};
