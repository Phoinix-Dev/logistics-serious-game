# Ship serious game
**A serious game to raise interest in logistics and teaching some fundamentals**

<img src="/Content/Resources/Image/Header.png" />

This is the source code of the Prototype. To use it, download the Unreal Engine 4.25 from Unrealengine.com. 

## Credits

The project uses the [BuoancyPlugin](https://github.com/UE4-OceanProject/BuoyancyPlugin/tree/627d8262e9ff3d636c70c0b50bdf94df406a3f43) and the [OceanPlugin](https://github.com/UE4-OceanProject/OceanPlugin/).

We use textures and assets from Quixel 

<img src="/Content/Resources/Image/QuixelAssets.png" width="500" /> 

The ship objects are from [Sktechfab](https://sketchfab.com/):

<img src="/Content/Resources/Image/SketchfabAssets.png" width="500" />


* [Bulk carrier lowpoly](https://sketchfab.com/3d-models/bulk-carriers-lowpoly-7d2f4b61602c4144a2f7b1d5aa2959bd) (Top left)
* [Mega Express TWO](https://sketchfab.com/3d-models/mega-express-two---corsica-ferries-7381db3a9a3c49879a14376434a4b0d3) (Top right)
* [Maersk container ship lowpoly](https://sketchfab.com/3d-models/maersk-container-ship-lowpoly-1633130489d14a2b8e9a323a0db33458) (Bottom left)
* [SUPER tanker TI_Class](https://sketchfab.com/3d-models/super-tanker-ti-class-028f797a989144968538ef6ec62f48b7) (Bottom right)


The ship engine audio is by IsraGallo available on [Freesound](https://freesound.org/s/514441/).
